import numpy as np
import torch
from main.util.pyimlib_imtools import color_transfer


def torch_extension(fun):
    """Define torch extension for numpy functions."""
    def wrapper(*args, **kwargs):
        list_arg = list(args)
        n_arg_torch = 0
        for n in range(len(list_arg)):
            arg = list_arg[n]
            if type(arg).__module__ is 'torch':
                arg_np = arg.cpu().detach().numpy()
                n_arg_torch += 1
            else:
                arg_np = arg
            list_arg[n] = arg_np

        res_np = fun(*list_arg, **kwargs)

        res = torch.from_numpy(np.array(res_np)) if (
            res_np is not None) & (n_arg_torch > 0) else res_np

        return res

    return wrapper


def ind2str(n, N):
    """Returns integer in a nice format."""
    if n == 0:
        n_digits = 0
    else:
        n_digits = int(np.log10(n))
    N_digits = int(np.log10(N))
    return '0' * (N_digits - n_digits) + str(n)


@torch_extension
def imread(filename):
    import tifffile
    from PIL import Image
    import numpy
    extension = filename[-4:]
    if extension == 'tiff':
        img = tifffile.imread(filename)
    else:
        img = Image.open(filename)
        img = numpy.array(img)
    return img


@torch_extension
def imsave(filename, x, x0=None, method=None):
    import tifffile

    if x0 is not None:  # is not None
        x_int = np.transpose(x[0], (1, 2, 0))
        x0_int = np.transpose(x0[0], (1, 2, 0))
        x_save = color_transfer(x, x0, method)
    else:
        x_save = x

    tifffile.imsave(filename, x_save)


def tiff2png(filename, newfilename=None):
    # for now only with 4d images (vgg outputs)
    import tifffile
    import os
    from PIL import Image
    import numpy as np
    if newfilename == None:
        filename_png = filename[0:-5] + '.png'
    else:
        filename_png = newfilename
    img = tifffile.imread(filename)
    if img.ndim == 4:
        img = np.transpose(img[0, :, :, :], (1, 2, 0))
    np.seterr(divide='ignore', invalid='ignore')
    SATURATION = 0.1
    im_min = np.percentile(img[:], SATURATION)
    im_max = np.percentile(img[:], 100 - SATURATION)
    img = np.clip((img - im_min) / (im_max - im_min), 0, 1)
    result = Image.fromarray((img * 255).astype(np.uint8))
    result.save(filename_png)


def list_files(path):
    from os import listdir
    from os.path import isfile, join
    files = [join(path, f)
             for f in sorted(listdir(path)) if isfile(join(path, f))]
    return files


def make_gif(img_dir, out_dir, gif_name='movie', **kwargs):
    import imageio
    import os
    images = []
    filenames = list_files(img_dir)
    for filename in filenames:
        images.append(imageio.imread(filename))
    path = out_dir + gif_name + '.gif'
    path_vid = out_dir + gif_name + '.mp4'
    imageio.mimsave(path, images, **kwargs)
    command = 'ffmpeg -r -f gif -i ' + path + ' ' + path_vid
    os.system(command)
