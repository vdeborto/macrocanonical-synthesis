import torch


def torch_c_mult(t1, t2):
    """Complex tensor multiplication."""
    t_real = t1[..., 0] * t2[..., 0] - t1[..., 1] * t2[..., 1]
    t_imag = t1[..., 0] * t2[..., 1] + t1[..., 1] * t2[..., 0]
    return torch.stack((t_real, t_imag), -1)


def torch_conj(t):
    """Complex tensor conjugation."""
    return torch.stack((t[..., 0], t[..., 1] * -1), -1)


def torch_sp(t1, t2):
    """Scalar product between tensors."""
    return torch.dot(t1.view(-1), t2.view(-1))


def torch_nsq(t):
    """Tensor of the square norm of complex tensor."""
    return t[..., 0] ** 2 + t[..., 1] ** 2


def rmse(x, y):
    """Root Mean Square Error (RMSE) between two tensors."""
    import numpy as np
    diff = x - y
    diff_np = diff.cpu().detach().numpy()
    norm_diff = np.linalg.norm(diff_np)
    error = norm_diff
    return error, diff


def nrmse(x, y):
    """Normalised RMSE between two tensors."""
    import numpy as np
    diff = x - y
    x_np = x.cpu().detach().numpy()
    diff_np = diff.cpu().detach().numpy()
    norm_x = np.linalg.norm(x_np)
    norm_diff = np.linalg.norm(diff_np)
    error = norm_diff / norm_x
    return error, diff
