import numpy as np
import torch
from main.util.processing_vgg import back2img, back2img_x0


def fill_parameter(params):
    if not('seed' in params.keys()):
        import time
        params['seed'] = int(time.time())
    if not('save' in params.keys()):
        params['save'] = None
        params['stride_save_image'] = 1
        params['stride_save_curve'] = 1
    if not('res_path' in params.keys()):
        params['res_path'] = None
    if not('true_size' in params.keys()):
        params['true_size'] = False
    if not('force' in params.keys()):
        params['force'] = False
    if not('burnin' in params.keys()):
        params['burnin'] = 1
    if not('weight' in params.keys()):
        params['weight'] = 0
    if not('color_transfer' in params.keys()):
        params['color_transfer'] = 'histogram_matching'
    if not('beta' in params.keys()):
        params['beta'] = 1

    return params


def header_coordinates(filename, **kwargs):
    """Insert header for Tikz pictures."""
    f = open(filename, 'a')
    string = kwargs.get('string', "")
    f.write('\\addplot+[mark=+, thick' + string + '] coordinates { \n \\}')
    f.close()


def header_general(folder_path, identifier, identifier_name, nmax, order):
    from main.util.pyimlib_save import ind2str
    filename = folder_path + identifier_name + '.tex'
    f = open(filename, 'a')
    f.write('\\begin{tikzpicture}[scale = 0.7] \n')
    f.write(
        '\\begin{axis}[grid=major,no markers,domain=-5:5,enlargelimits=false] \n ')
    stride = 1
    for j in range(0, nmax, stride):
        strj = '\\input{./order' + str(order) + '/' + \
            identifier + '_' + ind2str(j, nmax) + '.tex} \n'
        f.write(strj)
    f.write('\\end{axis} \n')
    f.write('\\end{tikzpicture} \n')
    f.close()


def add_line(tex_file, val, n_it):
    """Add line (n_it, val) in .tex file for Tikz picture."""
    f = open(tex_file, 'r')
    lines = f.readlines()
    f.close()
    f = open(tex_file, 'w')
    for line in lines[:-1]:
        f.write(line)
    f.write('(' + str(n_it) + ', ' + str(val) + ') \n }; \n')
    f.close()


def imsave_pngtiff(filename, x, x0=None, method=None, newfilename=None):
    from main.util.pyimlib_save import imsave, tiff2png
    imsave(filename, x, x0, method)
    tiff2png(filename, newfilename)


def save_image_general(params, x, x_grad, n_it, folder_name, im_number=None):
    """Save current image and basic image statistics."""
    from numpy import mod, min, max
    from numpy.linalg import norm
    from main.util.pyimlib_save import ind2str

    N_max = params['iteration_number']
    filename = params['res_path'] + folder_name
    px0 = back2img_x0(params, params['x0'])

    x0_np = px0.cpu().detach().numpy()
    x_np = x.cpu().detach().numpy()
    x_grad_np = x_grad.cpu().detach().numpy()

    if im_number is not(None):
        im_nb = '_' + ind2str(im_number, params['N_scale'])
    else:
        im_nb = ''

    prefix_tiff = filename + '/image/tiff/im' + im_nb
    name_x = prefix_tiff + '_it' + ind2str(n_it, N_max) + '.tiff'
    name_x_his = prefix_tiff + '_his_it' + ind2str(n_it, N_max) + '.tiff'
    name_x_g = prefix_tiff + '_grad_it' + ind2str(n_it, N_max) + '.tiff'
    prefix_png = filename + '/image/png/im' + im_nb
    name_x_png = prefix_png + '_it' + ind2str(n_it, N_max) + '.png'
    name_x_his_png = prefix_png + '_his_it' + ind2str(n_it, N_max) + '.png'
    name_x_g_png = prefix_png + '_grad_it' + ind2str(n_it, N_max) + '.png'
    if mod(n_it, params['stride_save_image']) == 0:
        imsave_pngtiff(name_x, x_np, newfilename=name_x_png)
        imsave_pngtiff(name_x_g, x_grad_np, newfilename=name_x_g_png)
        if params['feature_type'] == 'CNNFeature':
            method = params['color_transfer']
            imsave_pngtiff(
                name_x_his, x_np, px0, method=method, newfilename=name_x_his_png)

    if mod(n_it, params['stride_save_curve']) == 0:
        add_line(filename + '/data/im_max.tex', max(x_np), n_it)
        add_line(filename + '/data/im_min.tex', min(x_np), n_it)
        add_line(filename + '/data/im0_max.tex', max(x0_np), n_it)
        add_line(filename + '/data/im0_min.tex', min(x0_np), n_it)
        add_line(filename + '/data/im_norm.tex', norm(x_np), n_it)
        add_line(filename + '/data/im_grad_max.tex', max(x_np), n_it)
        add_line(filename + '/data/im_grad_min.tex', min(x_np), n_it)
        add_line(filename + '/data/im_grad_norm.tex', norm(x_grad_np), n_it)


def save_weight_vgg_feature(obj, params, feat, weight, weight_avg, n_it, n_epoch, folder_name):
    """Save current weights and basic weight statistics. (vgg features)"""
    from numpy import mod
    from main.util.pyimlib_save import ind2str
    filename = params['res_path'] + folder_name
    layer = obj.layer
    L = obj.nb_layer

    CHANNELS = [0, 10, 20, 30, 40]

    for l in range(L):
        l_str = ind2str(layer[l], layer[L - 1])

        weight_np = weight[l].cpu().detach().numpy()
        weight_avg_np = weight_avg[l].cpu().detach().numpy()

        if n_it == 0:
            for channel in CHANNELS:
                channel_str = ind2str(channel, max(CHANNELS))
                name_curve = '/data/w_' + l_str + '_' + channel_str + '.tex'
                name_curve_avg = '/data/w_avg_' + l_str + '_' + channel_str + '.tex'
                name_grad_curve = '/data/w_grad_' + l_str + '_' + channel_str + '.tex'
                header_coordinates(filename + name_curve)
                header_coordinates(filename + name_curve_avg)
                header_coordinates(filename + name_grad_curve)
                # same with epoch
                name_curve_ep = '/data/w_' + l_str + '_' + channel_str + '_epoch.tex'
                name_curve_avg_ep = '/data/w_avg_' + l_str + '_' + channel_str + '_epoch.tex'
                name_grad_curve_ep = '/data/w_grad_' + l_str + '_' + channel_str + '_epoch.tex'
                header_coordinates(filename + name_curve_ep)
                header_coordinates(filename + name_curve_avg_ep)
                header_coordinates(filename + name_grad_curve_ep)

        if mod(n_it, params['stride_save_curve']) == 0:
            feat_init = obj.feature_init
            diff = feat_init[l] - feat[l]

            diff_np = diff.cpu().detach().numpy()
            for channel in CHANNELS:
                channel_str = ind2str(channel, max(CHANNELS))
                name_curve = '/data/w_' + l_str + '_' + channel_str + '.tex'
                name_curve_avg = '/data/w_avg_' + l_str + '_' + channel_str + '.tex'
                name_grad_curve = '/data/w_grad_' + l_str + '_' + channel_str + '.tex'
                add_line(filename + name_curve, weight_np[channel], n_it)
                add_line(filename + name_curve_avg,
                         weight_avg_np[channel], n_it)
                add_line(filename + name_grad_curve, diff_np[channel], n_it)
                name_curve_ep = '/data/w_' + l_str + '_' + channel_str + '_epoch.tex'
                name_curve_avg_ep = '/data/w_avg_' + l_str + '_' + channel_str + '_epoch.tex'
                name_grad_curve_ep = '/data/w_grad_' + l_str + '_' + channel_str + '_epoch.tex'
                add_line(filename + name_curve_ep, weight_np[channel], n_epoch)
                add_line(filename + name_curve_avg_ep,
                         weight_avg_np[channel], n_epoch)
                add_line(filename + name_grad_curve_ep,
                         diff_np[channel], n_epoch)

    return


def save_general_info(params, obj, folder_name):
    import os
    import datetime
    from shutil import rmtree, copytree, move

    # define folder
    if not(params['res_path'] == None):
        prefix = params['res_path']
    else:
        params['res_path'] = './res_test/'
    filename = params['res_path'] + folder_name
    if os.path.exists(filename):
        if not(params['force']):
            decision = input('Folder already exists. Overwrite? (y|n)')
            if decision == 'y':
                rmtree(filename)
            else:
                raise Exception('Exit')
        else:
            rmtree(filename)
    os.makedirs(filename)
    os.makedirs(filename + '/image')
    os.makedirs(filename + '/image/tiff')
    os.makedirs(filename + '/image/png')
    os.makedirs(filename + '/weight')
    os.makedirs(filename + '/weight/tiff')
    os.makedirs(filename + '/weight/png')
    print('Result directory: ' + filename)

    copytree('./tex/', filename + '/data/')
    name_list = ['im_max', 'im_min', 'im_grad_max', 'im_grad_min',
                 'im0_min', 'im0_max', 'im_norm', 'im_grad_norm']
    move(filename + '/data/create_tex', filename + '/create_tex')
    for name in name_list:
        header_coordinates(filename + '/data/' + name + '.tex')

    if params['save'] == 'all':
        n_images = params['iteration_number'] / params['stride_save_image']
    elif params['save'] == 'light':
        n_images = 2
    else:
        n_images = 0
    if obj.x0.dim() > 1:
        [nx, ny] = [obj.x0.shape[-2], obj.x0.shape[-1]]

    n_feat = 0
    for l in range(len(obj.layer)):
        n_feat += obj.feature_init[l].shape[0]

    if obj.x0.dim() > 1:
        if 'size_out' in params:
            size_out = params['size_out']
            nx_out = size_out[0]
            ny_out = size_out[1]
        else:
            nx_out = nx
            ny_out = ny

    long_transition = '-' * 80
    short_transition = '-' * 40
    s = str(datetime.datetime.now()) + '\n\n\n'
    s += 'GENERAL DETAILS \n'
    s += 'folder name: ' + filename + '\n'
    s += 'seed: ' + str(params['seed']) + '\n'
    if obj.x0.dim() > 1:
        s += 'exemplar texture size: ' + str(nx) + 'x' + str(ny) + '\n'
        s += 'output texture size: ' + str(nx_out) + 'x' + str(ny_out) + '\n'
    s += 'number of features: ' + str(int(n_feat)) + '\n'
    s += 'number of generated samples: ' + str(int(n_images)) + '\n'
    s += 'time elapsed: ' '\n'
    s += 'size of the folder: '
    s += '\n \n' + long_transition + '\n'
    s += 'WEIGHT PARAMETERS (i.e gradient descent): \n'
    s += 'iteration number: ' + str(params['iteration_number']) + '\n'
    s += 'initial step size: ' + str(params['weight_step']) + '\n'
    s += 'Burnin: ' + str(params['burnin']) + '\n'
    s += '\n' + long_transition + '\n'
    s += 'MARKOV CHAIN PARAMETERS:' + '\n'
    s += 'Markov chain step size: ' + \
        str(params['image_step']) + '\n'
    s += 'Markov chain number of iterations: ' + \
        str(params['langevin_iterations']) + '\n'
    s += '\n' + long_transition + '\n'
    s += 'FEATURE PARAMETERS: \n'
    s += 'feature type: ' + params['feature_type'] + '\n'
    s += 'number of layers: ' + str(obj.nb_layer) + '\n'
    s += 'layer numbers: ('
    for k in range(obj.nb_layer):
        s += str(obj.layer[k]) + ', '
    s = s[:-2] + ') \n'

    f = open(filename + '/parameters.txt', 'w')
    f.write(s)
    f.close()


def save_init(params, Feature):
    import datetime
    from shutil import copyfile
    from main.util.pyimlib_save import tiff2png
    date = str(datetime.datetime.now())[0:10]
    folder_name = date + '_' + params['name']
    save_general_info(params, Feature, folder_name)
    return folder_name


def get_size(start_path='.'):
    import os
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size


def weight_max(x, weight, params, folder_name, Feature, perc):
    from main.util.pyimlib_save import ind2str
    from math import ceil
    import numpy as np
    sort_max_out = []
    name_max_out = []
    L = len(weight)
    for l in range(L):

        weight_np = weight[l].cpu().detach().numpy()
        sort_idx = np.argsort(np.abs(weight_np)[::-1])  # descending order
        nb_channel = len(sort_idx)
        nb_idx = ceil(perc / 100 * nb_channel)
        sort_max = sort_idx[0:nb_idx]
        name_max_out_int = []
        layer = params['layer']
        name_int = 'layer_' + ind2str(layer[l], max(layer)) + '_channel_'
        for k in sort_max:
            name_max_out_int.append(name_int + ind2str(k, nb_channel))
        name_max_out.append(name_max_out_int)
        sort_max_out.append(sort_max)

    feature_x = Feature.network.forward(x)
    feature_x0 = Feature.network.forward(params['x0'])
    name_x_path = params['res_path'] + folder_name + '/weight/tiff/im_'
    name_x0_path = params['res_path'] + folder_name + '/weight/tiff/init_im_'
    name_x_path_png = params['res_path'] + folder_name + '/weight/png/im_'
    name_x0_path_png = params['res_path'] + \
        folder_name + '/weight/png/init_im_'
    for l_id in range(L):
        for c_id in range(len(sort_max_out[l_id])):
            l = layer[l_id]
            c = sort_max_out[l_id][c_id]
            name_x = name_x_path + name_max_out[l_id][c_id] + '.tiff'
            name_x0 = name_x0_path + name_max_out[l_id][c_id] + '.tiff'
            name_x_png = name_x_path_png + name_max_out[l_id][c_id] + '.png'
            name_x0_png = name_x0_path_png + name_max_out[l_id][c_id] + '.png'
            feat_x = feature_x[l_id][0, c, :, :]
            feat_x0 = feature_x[l_id][0, c, :, :]
            imsave_pngtiff(name_x, feat_x, newfilename=name_x_png)
            imsave_pngtiff(name_x0, feat_x0, newfilename=name_x0_png)
    return


def save_weight_all_vgg(x, weight, params, folder_name, Feature):
    import matplotlib.pyplot as plt
    import numpy as np
    layer_tuple = params['layer']

    fe = Feature.feature_fun(params['x0'])
    we = weight
    filename_f = params['res_path'] + folder_name + '/data/feature_init'
    filename_w = params['res_path'] + folder_name + '/data/weight_final'
    torch.save(fe, filename_f)
    torch.save(we, filename_w)

    M_list = []
    m_list = []
    for l in range(len(layer_tuple)):

        M_list.append(weight[l].max().cpu().detach().numpy())
        m_list.append(weight[l].min().cpu().detach().numpy())
    VEPS = 0.2
    M = max(M_list) + VEPS  # maximum of weights
    m = min(m_list) - VEPS  # minimum of weights
    NB_PT = 150
    x_arr = np.linspace(m, M, NB_PT)

    for l in range(len(layer_tuple)):
        layer = layer_tuple[l]
        plt.plot(x_arr, x_arr * 0 + layer,
                 '--', color='darkorange')

        w_arr = weight[l].cpu().detach().numpy()
        plt.plot(w_arr, w_arr * 0 + layer, '|', color='blue')
    filename = params['res_path'] + folder_name + '/data/weight_out.png'
    plt.savefig(filename, format='png', dpi=1000)

    perc = 1
    weight_max(x, weight, params, folder_name, Feature, perc)
    return


def save_end(x, weight, params, folder_name, dt, Feature):
    import os
    filename = params['res_path'] + folder_name + '/parameters.txt'
    f = open(filename, 'r')
    lines = f.readlines()

    size_dir = get_size(params['res_path'] + folder_name)

    lines[8] = 'time elapsed: ' + '{:.2f}'.format(dt) + 's \n'
    lines[9] = 'size of the folder: ' + '{:.2E}'.format(size_dir) + 'B \n'

    f = open(filename, 'w')
    f.writelines(lines)
    f.close()

    save_weight_all_vgg(x, weight, params, folder_name, Feature)


def save_model(model, folder_name):
    import os
    import pickle
    params = model['params']
    filename = params['res_path'] + folder_name + '/model'
    with open(filename, 'wb') as file_model:
        pickle.dump(params, file_model)


def fetch_model(path):
    import pickle
    filename = path + 'model'
    with open(filename, 'rb') as file_model:
        model = pickle.load(file_model)
    return model


def load_input(path, params_update=None, x_init_update=None, last_w=False):
    model_init = fetch_model(path)
    params = model_init['params']
    x_init = model_init['x0']

    if params_update is not None:
        for k in params_update.keys():
            params[k] = params_update[k]

    if x_init_update is not None:
        x_init = x_init_update

    if last_w:
        params['weight'] = model_init['weight']

    return params, x_init, model_init


def default_params():
    params = {'feature_type': 'CNNFeature',
              'init': 'ADSN',
              'size_out': (512, 512),
              'model_cnn': 'vgg19',
              'layer': (1, 3, 6, 8, 11, 13, 15, 24, 26, 31),
              'regularization': 1,
              'weight_step': 10 ** -3,
              'image_step': 10 ** -5,
              'langevin_iterations': 1,
              'iteration_number': 3 * 10 ** 3,
              'burnin': 10 ** 3,
              'seed': 123,
              'save': 'all',
              'stride_save_image': 10 ** 3,
              'stride_save_curve': 10 ** 3,
              'beta': 512,
              'name': 'test',
              'filename': './im_test/rock.png',
              'force': True
              }
    return params


def get_args_cnn():
    import argparse
    params = default_params()

    parser = argparse.ArgumentParser(description="macrocanonical texture params",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--feature_type", type=str,
                        default=params['feature_type'])
    parser.add_argument("--init", type=str, default=params['init'])
    parser.add_argument("--size_out", nargs='+', type=int,
                        default=params['size_out'])
    parser.add_argument("--model_cnn", type=str, default=params['model_cnn'])
    parser.add_argument("--layer", nargs='+', type=int,
                        default=params['layer'])
    parser.add_argument("--regularization", type=float,
                        default=params['regularization'])
    parser.add_argument("--weight_step", type=float,
                        default=params['weight_step'])
    parser.add_argument("--image_step", type=float,
                        default=params['image_step'])
    parser.add_argument("--langevin_iterations", type=int,
                        default=params['langevin_iterations'])
    parser.add_argument("--iteration_number", type=int,
                        default=params['iteration_number'])
    parser.add_argument("--burnin", type=int,
                        default=params['burnin'])
    parser.add_argument("--save", type=str, default='all')
    parser.add_argument("--stride_save_image", type=int,
                        default=params['stride_save_image'])
    parser.add_argument("--stride_save_curve", type=int,
                        default=params['stride_save_image'])
    parser.add_argument("--beta", type=int, default=params['beta'])
    parser.add_argument("--name", type=str, default=params['name'])
    parser.add_argument("--filename", type=str, default=params['filename'])

    args = parser.parse_args()

    params['feature_type'] = args.feature_type
    params['init'] = args.init
    params['size_out'] = args.size_out
    params['model_cnn'] = args.model_cnn
    params['layer'] = args.layer
    params['regularization'] = args.regularization
    params['weight_step'] = args.weight_step
    params['image_step'] = args.image_step
    params['langevin_iterations'] = args.langevin_iterations
    params['iteration_number'] = args.iteration_number
    params['burnin'] = args.burnin
    params['save'] = args.save
    params['stride_save_image'] = args.stride_save_image
    params['stride_save_curve'] = args.stride_save_curve
    params['beta'] = args.beta
    params['name'] = args.name
    params['filename'] = args.filename

    return params
