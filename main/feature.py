"""Feature constraints.

This module introduces feature used in macrocanonical densities.
"""
import math
import torch
import numpy as np
from main.nnclass import CNN
from main.util.save_util import save_image_general, save_weight_vgg_feature


class CNNFeature:
    """
    Construct CNN feature methods.

    Attributes
    ----------
    name : str
        Name of the class, here 'CNNFeature'.
    layer : tuple
        A tuple corresponding to the layers to be considered in the log-likelihood.
    nb_layer : int
        The number of layers, corresponding to len(self.layer).
    model_cnn : str
        Name of the neural network architecture to be used.
    network : instance of a class
        The instance of the class is given by CNN, see nnclass.py for more details.
    beta : float
        The features are divided by this value.
        Be careful, the algorithm is sensitive with respect to this value.
    forward : a function
        A function taking a tensor as an input and returning the tensor of the output at given layers.
        See self.forward in nnclass for more details.
    x0 : torch.tensor
        4d torch tensor, the examplar image.
        First dimension is one.
        Second and third dimension correspond to the width and the height of the image.
        Fourth dimension is set to 3.
    feature_init : list
        A list of length self.nb_layer. Each element of feature_init is a tensor.
    weight : list
        Same structure as feature_init and contains weight to compute log-likelihood.
    regularization : float
        Inverse of the variance in the a priori white noise (or regularization parameter).

    Methods
    -------
    feature_fun(x)
        Compute features of x.
    log_likelihood(out, x)
        Compute log-likelihood associated with x and features out.
    weight_up(weight, out, step, weight_old, n_it)
        Update weight.
    weight_avg(weight, step, weight_mv_avg, sum_step)
        Update moving average on weights.
    save_image(params, x, x_grad, n_it, folder_name, im_number=None)
        Save images.
    save_weight(params, feat, w, w_avg, n_it, n_epoch, folder_name)
        Save weight statistics.
    """

    def __init__(self, params):
        self.name = 'CNNFeature'
        self.layer = params['layer']
        self.nb_layer = len(params['layer'])
        self.model_cnn = params['model_cnn']
        params_CNN = dict()
        params_CNN['layers'] = params['layer']
        params_CNN['model_str'] = params['model_cnn']
        self.beta = params['beta']
        self.network = CNN(params_CNN)
        self.forward = self.network.forward

        self.x0 = params['x0']
        self.feature_init = self.feature_fun(params['x0'])

        self.weight = params['weight']
        self.regularization = params['regularization']

    def feature_fun(self, x):
        feat = self.forward(x)
        x0 = self.x0
        out = []
        size_x = x.shape[-2] * x.shape[-1]
        size_x0 = x0.shape[-2] * x0.shape[-1]
        ratio_size = size_x0 / size_x
        for feat_l in feat:
            ratio_layer = feat_l.shape[-2] * feat_l.shape[-1]
            ratio_layer = size_x / ratio_layer
            ratio = ratio_layer * ratio_size / self.beta
            out.append(feat_l[0].sum((1, 2)) * ratio)
        return out

    def log_likelihood(self, out, x):
        weight = self.weight
        reg = self.regularization

        lik_f = 0

        out_lik = out.copy()
        for l in range(len(out_lik)):
            lik_f += torch.dot(weight[l], out_lik[l])

        lik = lik_f + reg * x.norm(2).pow(2) / 2
        return lik

    def weight_up(self, weight, out, step):
        weight_new = []
        feat_init = self.feature_init

        for l in range(len(weight)):
            w_n_l = weight[l] + step * (out[l] - feat_init[l])
            weight_new.append(w_n_l)
        return weight_new

    def weight_avg(self, weight, weight_mv_avg, sum_step):
        weight_new = []
        sum_step_new = sum_step + 1
        r = 1 / sum_step_new
        for l in range(len(weight)):
            weight_l = r * weight[l] + (1 - r) * weight_mv_avg[l]
            weight_new.append(weight_l)
        return weight_new, sum_step_new

    def save_image(self, params, x, x_grad, n_it, folder_name, im_number=None):

        save_image_general(params, x, x_grad, n_it,
                           folder_name, im_number)

    def save_weight(self, params, feat, w, w_avg, n_it, n_epoch, fd_name):
        save_weight_vgg_feature(self, params, feat, w,
                                w_avg, n_it, n_epoch, fd_name)


def feature_selection(params):
    feature = {
        'CNNFeature': lambda x: CNNFeature(x),
    }[params['feature_type']]
    return feature(params)
