"""Alternate minimization/sampling

This module implements the core function to synthesize new images.
It contains the following functions:
    * time_comp
    * build_model
    * alternate
"""
import math
import numpy as np
import torch
import time
import datetime
from tqdm import tqdm
from main.init import init_alternate
from main.feature import feature_selection
from main.util.save_util import save_init, save_end, save_model
from main.util.processing_vgg import back2img


def time_comp(fun):
    def wrapper(*args, **kwargs):
        date = str(datetime.datetime.now())[0:10]
        params = args[0]
        t0 = time.perf_counter()
        model = fun(*args, **kwargs)

        tf = time.perf_counter()
        dt = tf - t0

        # unpack
        params = model['params']
        x = model['x']
        w = model['weight']
        feature = model['feature']

        print('Time elapsed: ', '{:.2f}'.format(dt) + 's')
        fd_name = date + '_' + params['name']
        save_end(x, w, params, fd_name, dt, feature)
        save_model(model, fd_name)

        return model
    return wrapper


def build_model(params, x, x_grad, x0, w, w_avg, w0, feature):
    """
    Build output model for alternate function.

    Parameters
    ----------
    params: dict
        this dictionary contains all the parameter of the model.
        See the different test files for examples.
    x: torch.Tensor
        the output image.
    x_grad: torch.Tensor
        the output gradient.
    x0: torch.Tensor
        the input image.
    w:
        the output weights. Type depends on feature.
    w_avg:
        the output averaged weights. Type depends on feature.
    w_avg:
        the input weights. Type depends on feature.
    feature: class
        an instance of a class given by feature_selection
        (see feature module for more info).

    Returns
    -------
    model: dict
        this dictionary contains all the input/output data of
        the model.
    """

    model = {'params': params,
             'x': x,
             'x_grad': x_grad,
             'x0': x0,
             'weight': w,
             'weight_avg': w_avg,
             'weight0': w0,
             'feature': feature}

    return model


@time_comp
def alternate(params, x):
    """
    Perform sampling according to macrocanonical model.

    Parameters
    ----------
    params: dict
        this dictionary contains all the parameters of the model.
        See the different test files for examples.
    x: torch.Tensor
        the initial image.

    Returns
    -------
    model: dict
        see build_model.
    """
    # INITIALIZATION
    # fill missing parameters
    from main.util.save_util import fill_parameter
    params = fill_parameter(params)

    # setting seed for reproducibility
    torch.manual_seed(params['seed'])

    # saving parameter initialization
    n_stride_im = params['stride_save_image']
    n_stride_curve = params['stride_save_curve']
    n_stride = min(n_stride_im, n_stride_curve)
    ne = 0  # number of epochs
    n_langevin = params['langevin_iterations']
    w_s = params['weight_step']
    x_s = params['image_step']

    # initialization
    N_it = params['iteration_number']
    w = params['weight']
    w_avg, sm_w = w, 0
    sm_f = 0
    feature = feature_selection(params)
    feature_fun = feature.feature_fun

    # store initialization
    x0 = x
    w0 = w
    f_avg = feature_fun(x)
    f_avgg = feature_fun(x)

    # saving folder initialization
    fd = save_init(params, feature)

    # MAIN LOOP
    for nit in tqdm(range(N_it + 1)):
        cond = params['save'] == 'all' and (np.mod(nit, n_stride) == 0)
        x.requires_grad_()

        # WEIGHT UPDATE
        with torch.no_grad():
            w = feature.weight_up(w, f_avg, w_s)
            feature.weight = w
            condition_avg = nit > params['burnin'] or nit == 0

            # weight average
            if condition_avg:
                n_avg = max(nit - params['burnin'], 0)
                w_avg, sm_w = feature.weight_avg(w, w_avg, sm_w)

            # save weight
            if cond and condition_avg:
                feature.save_weight(params, f_avg, w, w_avg, nit, ne, fd)

        # IMAGE UPDATE
        f_avg, sm_i = params['weight'], 0  # zerroing
        ne += n_langevin
        for k in range(n_langevin):
            # compute feature
            f = feature_fun(x)
            L = feature.log_likelihood(f, x)

            # compute gradient
            x_grad = torch.autograd.grad(L, x)[0]

            # save image
            if cond and (k == 0):
                px = back2img(params, x)
                px_grad = back2img(params, x_grad)
                feature.save_image(params, px, px_grad, nit, fd)

            # update image
            with torch.no_grad():
                x_rand = torch.randn(x.shape, dtype=torch.float)
                x_rand = x_rand.cuda()
                x = x - x_s * x_grad + math.sqrt(2 * x_s) * x_rand
                f_avg, sm_i = feature.weight_avg(f, f_avg, sm_i)

            x.requires_grad_()

    # build final model
    model = build_model(params, x, x_grad, x0, w, w_avg, w0, feature)
    return model
