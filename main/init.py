"""Alternate minimization/sampling

This module implements the initalization functions to synthesize new images.
It contains the following functions:
    * adsn
    * init_alternate
"""
import numpy as np
import torch
from numpy.fft import fft2, ifft2
from torchvision.transforms import ToTensor
from main.nnclass import CNN
from main.util.save_util import fill_parameter
from main.feature import feature_selection

def adsn(x0, x, m, size):
    """Numpy ADSN"""
    shape_x = size[0]
    shape_y = size[1]

    shape_ref_x = x0.shape[-2]
    shape_ref_y = x0.shape[-1]
    if (shape_x < shape_ref_x) or (shape_y < shape_ref_y):
        printf('output size must be bigger than target size')
        return

    x_ref_np = np.zeros((3, shape_x, shape_y))
    x_ref_np[:, 0:shape_ref_x, 0:shape_ref_y] = x0
    # TODO : periodic component + smoothing
    # REFERENCE : Random Phase Texture: Theory and Synthesis

    x_adsn_fft = fft2(x, axes=(-2, -1)) * fft2(x_ref_np, axes=(-2, -1))
    x_adsn_np = np.real(ifft2(x_adsn_fft, axes=(1, 2)))
    x_adsn_np = x_adsn_np / (np.sqrt(shape_x * shape_y))
    m_np = np.expand_dims(np.expand_dims(m, axis=-1), axis=-1)
    x_adsn_np = m_np + x_adsn_np
    return x_adsn_np


def init_alternate(params):
    """
    Perform initialization with given parameters.

    Parameters
    ----------
    params : dict
        this dictionary should contain the following fields:
        size_out : a tuple or a list containing the output size.
        feature_type : a string containing the type of feature
            (see feature for more info).
        init : a string 'white_noise' or 'ADSN' depending on the
            initialization chosen.

    Returns
    -------
    params : dict
        same dictionary as input with updated fields for
        'init', 'weight' and 'x0'.
    x_init : torch.Tensor
        initialized tensor.
    """

    # fill missing parameters
    params = fill_parameter(params)

    # opening image and scaling.
    bool_filename = False
    if 'filename' in params.keys():
        from main.util.processing_vgg import preprocessing_vgg
        from PIL import Image
        filename = params['filename']
        x0 = preprocessing_vgg(Image.open(filename))
        x0 = torch.stack([x0]).float()
        x0_np = x0.detach().cpu().numpy()
        m0 = np.mean(x0_np, (-2, -1))
        x0 = x0.cuda()
    elif 'x0' in params.keys():
        x0_np = params['x0'].cpu().detach().numpy()
        x0_np = (x0_np - x0_np.min()) / (x0_np.max() - x0_np.min())
        m0 = np.mean(x0_np, (0, 1))
        x0 = torch.stack([ToTensor()(x0_np)]).float()
        x0_np = x0_np - m0
        x0 = x0.cuda()
    else:
        printf('at least x0 or filename should filled in params')

    # shaping parameters + white noise initialization
    shape_x = params['size_out'][0]
    shape_y = params['size_out'][1]

    if 'seed' in params.keys():
        np.random.seed(params['seed'])

    # image initalization
    if params['init'] == 'white_noise':
        x_np = np.random.randn(3, shape_x, shape_y)
        x = torch.stack([ToTensor()(x_np)]).float()
    elif params['init'] == 'ADSN':
        x_np = np.repeat(np.random.randn(1, shape_x, shape_y), 3, 0)
        x_adsn_np = adsn(x0_np, x_np, m0, params['size_out'])
        x = torch.from_numpy(x_adsn_np).float()
    elif params['init'] == 'image':
        from main.util.processing_vgg import preprocessing_vgg
        from PIL import Image
        filename = params['init_filename']
        x = preprocessing_vgg(Image.open(filename))
        x = torch.stack([x]).float()
        x_np = x.detach().cpu().numpy()
    else:
        printf('init field in params should be white_noise, ADSN or image')

    x_init = x.cuda()

    # weight initialization
    params_CNN = dict()
    layer = params['layer']
    params_CNN['layers'] = layer
    params_CNN['model_str'] = params['model_cnn']
    network = CNN(params_CNN)
    
    w = []
    for l in range(len(layer)):
        model_l = list(network.model)[layer[l] - 1]
        w_l = torch.from_numpy(np.zeros(model_l.out_channels)).float()
        w_l = w_l.cuda()
        w.append(w_l)
    
    params.update({'x0': x0})
    params.update({'weight': w})

    return params, x_init
