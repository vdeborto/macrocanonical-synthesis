import matplotlib
matplotlib.use('Agg')  # no interactive backend

"""Testing vgg feature/macrocanonical model"""
import time
import torch
import numpy as np
from main.alternate import *
from main.feature import CNNFeature
from main.util.save_util import get_args_cnn

params = get_args_cnn()
params, x_init = init_alternate(params)
model = alternate(params, x_init)
