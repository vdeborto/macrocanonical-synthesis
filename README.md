# Maximum entropy methods for texture synthesis

This page contains the Python implementation used in the experimental
part of "[Maximum entropy methods for texture synthesis: theory and practice](https://arxiv.org/abs/1912.01691.pdf)", see also (https://vdeborto.github.io/publication/texture_soul/).  
Authors:  
-[Valentin De Bortoli](https://vdeborto.github.io/)   
-[Agnès Desolneux](https://desolneux.perso.math.cnrs.fr/)  
-[Alain Durmus](http://alain.perso.math.cnrs.fr/)  
-[Bruno Galerne](https://www.idpoisson.fr/galerne/)  
-[Arthur Leclaire](https://www.math.u-bordeaux.fr/~aleclaire/)

You can also consult the [google collab notebook](https://colab.research.google.com/drive/18TnAo5VCnnSv6zY4_t_5mdPwtpgjp0Xx#scrollTo=7e6XtairZ4TB).

## Requirements
-Python 3.3+   
-pyfftw (https://pypi.org/project/pyFFTW/)  
-torch (https://pytorch.org/)   
-torchvision (https://pytorch.org/docs/stable/torchvision/index.html)   
-tqdm (https://github.com/tqdm/tqdm)   
-tifffile (https://pypi.org/project/tiffile/2018.9.27/)

To install the requirements

$ python3 -m pip install -r requirements.txt

## Usage

To sample from the macrocanonical estimated from an image run

$ python3 test.py --filename /path/to/image/ --name name_folder

This code builds the macrocanonical model associated with the image
'path/to/image' (png image) and produce samples from this macrocanonical
following the Stochastic Optimization with Unadjusted Langevin (SOUL)
procedure described in our paper. The results are stored in
'./res_test/date_of_day_name_folder'.

In this folder you can:

(1) observe the samples 'date_of_day_name_folder/image/png' or 'date_of_day_name_folder/image/tiff'.
The sequence of images is stored as im_itxx.png or im_his_itxxx.png (with histogram equalization).

(2) observe the sequence of weights estimated by the algorithm.  
In 'date_of_day_name_folder/' run   
$ bash create_tex   
which outputs a pdf file containing the estimated weights.   

The parser admits other options such as:   
--init: 'white_noise' or 'ADSN' depending on the chosen initialization (default = 'ADSN')  
--size_out: a couple of integers which control the output size (default = (256, 256))   
--model_cnn: the choice of the neural network (default = 'vgg19')   
--layer: the choice of layers in the neural network (default = (1, 3, 6, 8, 11, 13, 15, 24, 26, 31))   
--regularization: the regularization parameter (default = 1)   
--weight_step: the step in the stochastic gradient procedure (default = 10 ** -3)   
--image_step: the step in the unadjusted Langevin procedure (default = 10 ** -5)   
--langevin_iterations: the number of unadjusted Langevin iterations (default = 1)   
--iteration_number: the total number of iterations (default = 3 * 10 ** 3)   
--burnin: the burnin of the algorithm (default = 10 ** -3)   
--seed: the random seed for reproducible results (default = 123)   
--save: 'all' or 'light' (default = 'all')   
--stride_save_image: the stride for saving the images (default = 10 ** 3)   
--stride_save_curve: the stride for saving the weights (default = 10 ** 3)  
--beta: a taming parameter (default = 256)   
--name: name of the output repository (default = test)   
--filename: path to the observation (default = './im_test/rock.png')   

## Examples

This repository also includes several examples you can directly run:

$ python3 test.py --filename ./im_test/rock.png --name rock  
$ python3 test.py --filename ./im_test/flower.png --name flower  
$ python3 test.py --filename ./im_test/sweet.png --name sweet  
$ python3 test.py --filename ./im_test/bark.png --name bark  
$ python3 test.py --filename ./im_test/coffee.png --name coffee  


For any questions, please contact Valentin De Bortoli (valentin.debortoli@gmail.com).